class profile::web {
    include stdlib

    # Packages
    package { "apache2": ensure => installed }
    package { "php": ensure => installed }

    # Root user account
    user{root :
            ensure => present,
            password => pw_hash('webpassword', 'SHA-512', 'mysalt'),
    }

    # Apache2 conf file.
    file { '/etc/apache2/apache2.conf':
            source  => 'puppet:///files/apache2-files/apache2.conf',
            ensure  => file,
            replace => true,
            force   => true,
    } 

    # php.ini
    file { '/etc/php/7.0/cli/php.ini':
            source  => 'puppet:///files/php-files/php.ini',
            ensure  => file,
            replace => true,
            force   => true,
    } 
    
}
