
class profile::database {

        # Install packages
        package { "mysql-server": ensure => installed }

        # Place config files
        file { '/etc/mysql/my.cnf':
                source  => 'puppet:///files/mysql-files/my.cnf',
        }
  
        # Change root user account password
        user {root :
                ensure => present,
                password => pw_hash('databasepassword', 'SHA-512', 'mysalt'),
        }

}
