class profile::base {
  include stdlib

    user { 'sbrunton':
 			 ensure           => 'present',
       home             => '/home/sbrunton',
       password         => pw_hash('sbrunton', 'SHA-512', 'mysalt'),
       password_max_age => '99999',
       password_min_age => '0',
       shell            => '/bin/bash',
       uid              => '600',
     }

    user { 'jmurray':
 			 ensure           => 'present',
       home             => '/home/jmurray',
       password         => pw_hash('jmurray', 'SHA-512', 'mysalt'),
       password_max_age => '99999',
       password_min_age => '0',
       shell            => '/bin/bash',
       uid              => '601',
     }

    user { 'mcampbell':
      ensure           => 'present',
      home             => '/home/mcampbell',
      password         => pw_hash('mcampbell', 'SHA-512', 'mysalt'),
      password_max_age => '99999',
      password_min_age => '0',
      shell            => '/bin/bash',
      uid              => '602',
     }

}
