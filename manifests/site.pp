# Default Node
node default {
  include stdlib
}

# Master Server
node 'puppet.master' {
  include role::master_server
}

# Web Group
node /^web/ {
  include role::web_server
}

# Database Group
node /^db/ {
  include role::database_server
}
